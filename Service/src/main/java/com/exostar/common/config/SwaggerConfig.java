package com.exostar.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableWebMvc
public class SwaggerConfig implements WebMvcConfigurer {
// http://localhost:8090/apipoc/exo-ui.html	
	ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("API Documentation")
            .description("The platform is a modernization/refactor effort of current Exostar IAM products.")
            .license("Exostar 3.0")
            .version("1.0.0")
            .build();
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)  
                .select()                                  
                .apis(RequestHandlerSelectors.basePackage("com.exostar.common.facade"))              
                //.paths(PathSelectors.any())                          
                .build()
                .apiInfo(apiInfo()); 
    }

   @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
            registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
    
   /* @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("exo-ui.html").addResourceLocations("classpath:/customui/");
            registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/customui/webjars/");
    }*/
  
}
