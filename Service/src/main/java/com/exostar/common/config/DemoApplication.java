package com.exostar.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.javadoc.configuration.JavadocPluginConfiguration;


@SpringBootApplication
@ComponentScan({"com.exostar.common.config","com.exostar.common.facade","com.exostar.iam.service","com.exostar.iam.dao"})
@EntityScan("com.exostar.iam.dao.entity")
@EnableJpaRepositories("com.exostar.iam.dao")
@ImportResource({"classpath*:applicationContext.xml"})
@Import(JavadocPluginConfiguration.class)
@PropertySource("classpath:application.properties")
public class DemoApplication extends SpringBootServletInitializer {
	
	@Value("${initParam.ConfigName}")
	private String initParamConfigName;
	
	@Value("${initParam.ConfigValue}")
	private String initParamConfigValue;

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(DemoApplication.class);
    }
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	/*@Bean
	public ServletRegistrationBean<ConfigServlet> servletRegistrationBean(){
	    ServletRegistrationBean<ConfigServlet> servletRegistrationBean = new ServletRegistrationBean<ConfigServlet>(new ConfigServlet(), false);
	    
		servletRegistrationBean.addInitParameter(initParamConfigName, initParamConfigValue);
	    servletRegistrationBean.addInitParameter("is_sdk", "true");
	    servletRegistrationBean.setLoadOnStartup(2);
		return servletRegistrationBean;
	}
	*/
}
