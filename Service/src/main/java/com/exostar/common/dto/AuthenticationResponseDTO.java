package com.exostar.common.dto;

public class AuthenticationResponseDTO {
	
	private boolean authenticationSuccessful;

	public boolean isAuthenticationSuccessful() {
		return authenticationSuccessful;
	}

	public void setAuthenticationSuccessful(boolean authenticationSuccessful) {
		this.authenticationSuccessful = authenticationSuccessful;
	}
	
	

}
