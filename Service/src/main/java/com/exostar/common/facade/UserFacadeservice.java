package com.exostar.common.facade;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.exostar.common.SystemUtil;
import com.exostar.common.dto.AuthenticationRequestDTO;
import com.exostar.common.dto.AuthenticationResponseDTO;
import com.exostar.iam.service.login.api.UserService;

import io.swagger.annotations.ApiOperation;

/**
 * @author alampallyv
 * UserFacadeservice Class Describes The Services.
 */
@RestController
@RequestMapping("rsapi")
public class UserFacadeservice {

	private static final Logger logger = LoggerFactory.getLogger(UserFacadeservice.class);

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/hi", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String hiThere() {
		logger.debug("Hi Testing ");
		return "Version 1 Date: " + new Date();
	}

	/**
	 * This method describes the functioning of verifyCredentials()
	 * @param authenticationRequestDTO authenticationRequestDTO is the input for verifyCredentials()
	 * @return authenticationResponseDTO is the output for verifyCredentials()
	 */
	@RequestMapping(value = "/user/login", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody AuthenticationResponseDTO verifyCredentials(
			@RequestBody AuthenticationRequestDTO authenticationRequestDTO) {
		return userService.verifyCredentials(authenticationRequestDTO);
	}
	
	@RequestMapping(value = "/test/{upn}", method = RequestMethod.GET,  produces = "application/json")
	@ApiOperation(value = "Takes UPN and retrives User's Full Name", notes = "Takes UPN and retrives User's Full Name. UPN is a Path Variable")
	public String printUser(@PathVariable("upn") String upn) {
		logger.debug("Print User.......... ");
		String decodedUpn = SystemUtil.decodeString(upn); 
		return this.userService.printUser(decodedUpn);
	}
}
