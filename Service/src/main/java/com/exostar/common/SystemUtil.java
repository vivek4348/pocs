package com.exostar.common;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class SystemUtil {

	public static String decodeString(String string) {
        if (string == null)
            return "";
        String result = null;
        try {
            result = URLDecoder.decode(string, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("UTF-8 not supported", ex);
        }
        return result;
    }

}
