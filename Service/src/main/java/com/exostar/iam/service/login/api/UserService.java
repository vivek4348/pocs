package com.exostar.iam.service.login.api;

import com.exostar.common.dto.AuthenticationRequestDTO;
import com.exostar.common.dto.AuthenticationResponseDTO;

public interface UserService {

	public AuthenticationResponseDTO verifyCredentials(AuthenticationRequestDTO authenticationRequestDTO);
	
	public String printUser(String username);
	
}
